using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SIMPVSync.Authentication;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class ApiKeyAuthenticationAttribute : Attribute, IAuthorizationFilter
{
    private const string ApiKeyHeaderName = "X-Api-Key";

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        if (!IsApiKeyValid(context.HttpContext))
        {
            context.Result = new UnauthorizedResult();
        }
    }
    
    private static bool IsApiKeyValid(HttpContext context)
    {
        string? apiKey = context.Request.Headers[ApiKeyHeaderName];
        
        if (string.IsNullOrWhiteSpace(apiKey))
        {
            return false;
        }

        var availableApiKeys = context.RequestServices
            .GetRequiredService<IConfiguration>()
            .GetSection("ApiKeys")
            .Get<string[]>();

        if (availableApiKeys is null || availableApiKeys.Length == 0)
        {
            return false;
        }
        
        return availableApiKeys.Contains(apiKey);
    }
}