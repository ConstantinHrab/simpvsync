using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using SIMPVSync.Utils;

namespace SIMPVSync.Models;

public class FraudNotification
{
    [Required]
    public Guid guid { get; set; }
    
    [Required]
    public string created_at { get; set; }
    
    [NotMapped]
    public DateTime CreatedAt => DateTime.ParseExact(created_at, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    
    [Required]
    public FraudType type { get; set; }
    
    [Required]
    public Precinct precinct { get; set; }
    
    [Required]
    public Voter voter { get; set; }
    
    public string? observations { get; set; }
    
    [Required]
    public Election election { get; set; }
    
    [Required]
    public List<Event> events { get; set; }
}