using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SIMPVSync.Models;

public class EventRequest
{
    public FraudNotification fraud_notification { get; set; }
}