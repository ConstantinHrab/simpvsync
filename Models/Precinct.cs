using System.ComponentModel.DataAnnotations;

namespace SIMPVSync.Models;

public class Precinct
{
    [Required]
    public int? id { get; set; }
    
    public int? nr { get; set; }
    
    public County? county { get; set; }
    
    public string? uat { get; set; }
    
    public string? locality { get; set; }
    
    public string? name { get; set; }
    
    public string? address { get; set; }
    
    [Required]
    public List<string> phones { get; set; }
}

public class County
{
    public string code { get; set; }
    public string name { get; set; }
}