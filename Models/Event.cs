using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using SIMPVSync.Utils;

namespace SIMPVSync.Models;

public class Event
{
    [Required]
    public ActionType type { get; set; }
    
    [Required]
    public string mode { get; set; }
    
    [NotMapped]
    public CnpType CnpMode => (CnpType)Enum.Parse(typeof(CnpType), mode);
    
    [Required]
    public int sms { get; set; }
    
    [Required]
    public bool offline { get; set; }
    
    [Required]
    public int? precinct_id { get; set; }
    
    public string? county { get; set; }
    
    public string? precinct_nr { get; set; }
    
    public string? precinct_name { get; set; }
    
    public string? precinct_address { get; set; }
    
    [Required]
    public List<string> phones { get; set; }
    
    [Required]
    public string added_at { get; set; }
    
    [NotMapped]
    public DateTime AddedAt => DateTime.ParseExact(added_at, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    
    public string? validated { get; set; }
    
    [NotMapped]
    public ValidatedType? ValidatedValue => validated is null ? null : (ValidatedType?)Enum.Parse(typeof(ValidatedType), validated);
    
    public string? validated_at { get; set; }
    
    [NotMapped]
    public DateTime? ValidatedAt => validated_at is null ? null : (DateTime?)DateTime.ParseExact(validated_at, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
}