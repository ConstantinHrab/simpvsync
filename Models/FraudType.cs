using SIMPVSync.Utils;

namespace SIMPVSync.Models;

public class FraudType
{
    public FraudNotificationType code { get; set; }
    public string name { get; set; }
}