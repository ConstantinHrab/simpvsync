using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using SIMPVSync.Utils;

namespace SIMPVSync.Models;

public class Voter
{
    public string? list_type { get; set; }

    [NotMapped]
    public VoterListType? ListType =>
        list_type != null ? (VoterListType)Enum.Parse(typeof(VoterListType), list_type) : null;
    
    public string? page_position { get; set; }
    
    public string? rights_revoked { get; set; }
    
    [Required]
    public string cnp { get; set; }
    
    public string? first_name { get; set; }
    
    public string? last_name { get; set; }

    public string? birth_date { get; set; }

    [NotMapped]
    public DateTime? BirthDate => birth_date != null
        ? DateTime.ParseExact(birth_date, "dd/MM/yyyy", CultureInfo.InvariantCulture)
        : null;
    
    public string? citizenship { get; set; }
}