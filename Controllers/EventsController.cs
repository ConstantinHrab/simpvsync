using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SIMPVSync.Authentication;
using SIMPVSync.Database;
using SIMPVSync.Models;
using SIMPVSync.Utils;

namespace SIMPVSync.Controllers;

[ApiController]
[Route("[controller]")]
public class EventsController(
    SimieopDbContext context
    ) : ControllerBase
{
    [HttpPost("NewEvent")]
    [ApiKeyAuthentication]
    public async Task<IActionResult> NewEvent(EventRequest model)
    {
        
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var notification = await context.D_NOTIFICARI_SECTII.Where(x => x.ID == model.fraud_notification.guid)
            .Include(x => x.Events).FirstOrDefaultAsync();
        
        // Add new notification
        if (notification == null)
        {   
            //Check if election exists
            var electionKeyExists = ElectionType.ElectionMapping.TryGetValue(model.fraud_notification.election.code, out var electionId);
            
            // Create voter
            var existingVoter = await context.A_Alegatori_Simpv.FirstOrDefaultAsync(x=> x.Cnp == model.fraud_notification.voter.cnp);
            
            if (!electionKeyExists)
            {
                return BadRequest("Invalid election code!");
            }

            if (existingVoter != null)
            {
                if (context.D_NOTIFICARI_SECTII.Include(x=> x.Persoana).Any(x =>
                        x.Persoana.Cnp == model.fraud_notification.voter.cnp && x.ID_Z_ALEGEGERI == electionId))
                {
                    return BadRequest("Voter already exists on another notification for this election!");
                }
            }
            
            var voterModel = new VoterModel()
            {
                TipLista = model.fraud_notification.voter.ListType,
                PozitiePagina = model.fraud_notification.voter.page_position,
                DrepturiRevocate = model.fraud_notification.voter.rights_revoked,
                Cnp = model.fraud_notification.voter.cnp,
                Prenume = model.fraud_notification.voter.first_name,
                Nume = model.fraud_notification.voter.last_name,
                DataNasterii = model.fraud_notification.voter.BirthDate,
                Cetatenie = model.fraud_notification.voter.citizenship
            };
            await context.A_Alegatori_Simpv.AddAsync(voterModel);
            
            var notifModel = new NotificationModel()
            {
                XDM_FID = model.fraud_notification.guid,
                ID = model.fraud_notification.guid,
                ID_Z_SECTIE_VOTARE = model.fraud_notification.precinct.id,
                DESCRIERE = model.fraud_notification.observations,
                DATA_CREAT = model.fraud_notification.CreatedAt,
                DATA_MODIFICAT = model.fraud_notification.CreatedAt,
                ID_Z_ALEGEGERI = electionId,
                TipNotificare = model.fraud_notification.type.code,
                NrSectie = model.fraud_notification.precinct.nr,
                JudetScurt = model.fraud_notification.precinct.county?.code,
                Judet = model.fraud_notification.precinct.county?.name,
                Uat = model.fraud_notification.precinct.uat,
                Localitate = model.fraud_notification.precinct.locality,
                NumeSectie = model.fraud_notification.precinct.name,
                AdresaSectie = model.fraud_notification.precinct.address,
                TelefonSectie = string.Join(",", model.fraud_notification.precinct.phones),
                Persoana = voterModel,
            };
            await context.D_NOTIFICARI_SECTII.AddAsync(notifModel);

            // Create events
            foreach (var eventModel in model.fraud_notification.events)
            {
                var eventModelDb = new EventModel()
                {
                    Tip = eventModel.type,
                    TipScanare = eventModel.CnpMode,
                    Sms = eventModel.sms,
                    Offline = eventModel.offline,
                    Notificare = notifModel,
                    DataCreare = eventModel.AddedAt,
                    // Detalii sectie
                    IdSectie = eventModel.precinct_id ?? 0,
                    Judet = eventModel.county,
                    NrSectie = eventModel.precinct_nr != null ? int.Parse(eventModel.precinct_nr) : null,
                    NumeSectie = eventModel.precinct_name,
                    AdresaSectie = eventModel.precinct_address,
                    Telefon = string.Join(',', eventModel.phones),
                    
                };

                if (eventModel.type == ActionType.Validated)
                {
                    eventModelDb.Validat = eventModel.ValidatedValue;
                    eventModelDb.DataValidare = eventModel.ValidatedAt;
                }
                
                await context.A_Evenimente_Simpv.AddAsync(eventModelDb);
            }
            
            await context.SaveChangesAsync();
        }
        else
        {
            notification.TipNotificare = model.fraud_notification.type.code;

            // Clear events (to remove if only new events are sent)
            notification.Events.Clear();

            foreach (var eventModel in model.fraud_notification.events)
            {
                
                
                var eventModelDb = new EventModel()
                {
                    Tip = eventModel.type,
                    TipScanare = eventModel.CnpMode,
                    Sms = eventModel.sms,
                    Offline = eventModel.offline,
                    Notificare = notification,
                    DataCreare = eventModel.AddedAt,
                    // Detalii sectie
                    IdSectie = eventModel.precinct_id ?? 0,
                    Judet = eventModel.county,
                    NrSectie = eventModel.precinct_nr != null ? int.Parse(eventModel.precinct_nr) : null,
                    NumeSectie = eventModel.precinct_name,
                    AdresaSectie = eventModel.precinct_address,
                    Telefon = string.Join(',', eventModel.phones),
                };

                if (eventModel.type == ActionType.Validated)
                {
                    eventModelDb.Validat = eventModel.ValidatedValue;
                    eventModelDb.DataValidare = eventModel.ValidatedAt;
                }
                
                notification.Events.Add(eventModelDb);
            }

            context.D_NOTIFICARI_SECTII.Update(notification);
            await context.SaveChangesAsync();
        }
        
        return Ok("Success!");
    }
    
    [HttpPost("getevents")]
    public IActionResult GetEvents()
    {
        return Ok();
    }
}