using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SIMPVSync.Utils;

namespace SIMPVSync.Database;

public class NotificationModel
{
    public int? XDM_FNO { get; set; } = 4100;
    
    public Guid? XDM_FID { get; set; }

    public int? XDM_CNO { get; set; } = 410001;

    public int? XDM_CID { get; set; } = 1;
    
    [Key]
    public Guid ID { get; set; }
    
    public int? ID_Z_JUDET { get; set; }
    
    public int? ID_Z_UAT { get; set; }

    public string? ID_SECTIE_VOTARE { get; set; } = null;
    public int? ID_Z_SECTIE_VOTARE { get; set; }
    
    public string? DESCRIERE { get; set; }

    public Guid? ID_Z_STATUS_NOTIFICARE { get; set; }= Guid.Parse("947c8f70-2262-4f68-b4b0-203a1a7025ad");

    public string? OBSERVATII { get; set; } = null;
    
    public DateTime? DATA_CREAT { get; set; }
    
    public DateTime? DATA_MODIFICAT { get; set; }

    public Guid? USER_CREAT { get; set; } = Guid.Parse("d18cd8c9-e08e-4a3b-8051-c5d4bf15a178");
    
    public Guid? USER_MODIFICAT { get; set; } = Guid.Parse("d18cd8c9-e08e-4a3b-8051-c5d4bf15a178");

    public string? IP_CREAT { get; set; } = "SIMPV";

    public string? IP_MODIFICAT { get; set; } = "SIMPV";

    public string? NRTRANZ { get; set; } = null;

    public bool? RETRAS { get; set; } = false;

    public Guid? STRUCTURA_CREAT { get; set; } = Guid.Parse("10D0CEAE-1495-413B-A883-07D194BDE1D9");
    
    public Guid? STRUCTURA_MODIFICAT { get; set; } = Guid.Parse("10D0CEAE-1495-413B-A883-07D194BDE1D9");

    public bool? DECLANSATOR_VERS { get; set; } = null;

    public Guid? ID_Z_JUDET_COMPETENTA { get; set; } = null;

    public Guid? ID_Z_ALEGEGERI { get; set; }

    public Guid? ID_Z_TIP_NOTIFICARE_ALEGERI { get; set; } = null;

    public Guid? XDM_FID_EVENIMENT { get; set; } = null;

    public Guid? COD_NOTIF { get; set; } = null;
    
    [Required]
    public FraudNotificationType TipNotificare { get; set; }
    
    public int? NrSectie { get; set; }
    
    public string? JudetScurt { get; set; }
    
    public string? Judet { get; set; }
    
    public string? Uat { get; set; }
    
    public string? Localitate { get; set; }
    
    public string? NumeSectie { get; set; }
    
    public string? AdresaSectie { get; set; }
    
    public string? TelefonSectie { get; set; }
    
    [ForeignKey("IdPersoana")]
    [Required]
    public virtual VoterModel Persoana { get; set; }
    
    
    public ICollection<EventModel> Events { get; set;  }
}