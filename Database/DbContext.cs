using Microsoft.EntityFrameworkCore;

namespace SIMPVSync.Database;

public class SimieopDbContext : DbContext
{
    public SimieopDbContext(DbContextOptions
        <SimieopDbContext> options)
        : base(options)
    {
        
    }
    
    public virtual DbSet<NotificationModel> D_NOTIFICARI_SECTII { get; set; }
    public virtual DbSet<EventModel> A_Evenimente_Simpv { get; set; }
    public virtual DbSet<VoterModel> A_Alegatori_Simpv { get; set; }
}