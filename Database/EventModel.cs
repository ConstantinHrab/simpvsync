using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SIMPVSync.Utils;

namespace SIMPVSync.Database;

public class EventModel
{
    [Key]
    public int Id { get; set; }
    
    [Required]
    public ActionType Tip { get; set; }
    
    [Required]
    public CnpType TipScanare { get; set; }
    
    [Required]
    public int Sms { get; set; }
    
    [Required]
    public bool Offline { get; set; }
    
    public int IdSectie { get; set; }
    
    public string? Judet { get; set; }
    
    public int? NrSectie { get; set; }
    
    public string? NumeSectie { get; set; }
    
    public string? AdresaSectie { get; set; }
    
    public string? Telefon { get; set; }
    
    [Required]
    public DateTime DataCreare { get; set; }
    
    public ValidatedType? Validat { get; set; }
    
    public DateTime? DataValidare { get; set; }
    
    
    [Required]
    [ForeignKey("IdNotificare")]
    public virtual NotificationModel Notificare { get; set; }
}