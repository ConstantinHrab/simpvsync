using System.Collections;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using SIMPVSync.Utils;

namespace SIMPVSync.Database;

[Index(nameof(Cnp), IsUnique = true)]
public class VoterModel
{
    [Key]
    public int Id { get; set; }
    
    public VoterListType? TipLista { get; set; }
    
    public string? PozitiePagina { get; set; }
    
    public string? DrepturiRevocate { get; set; }
    
    [Required]
    public string Cnp { get; set; }
    
    public string? Prenume { get; set; }
    
    public string? Nume { get; set; }
    
    public DateTime? DataNasterii { get; set; }
    
    public string? Cetatenie { get; set; }
    
    public ICollection<NotificationModel> Notifications { get; set; }
}