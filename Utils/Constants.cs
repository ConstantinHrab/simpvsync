namespace SIMPVSync.Utils;

public enum FraudNotificationType
{
    DoubleSectionVote = 1,
    DoubleVoteForSamePerson,
    PersonNotInVotersList,
    NoVotingRights,
}

public enum VoterListType
{
    Permanent = 1,
    Complementary,
    NoVotingRights,
    Corespondent,
    AnotherCountry,
    Special,
    Crds
}

public enum ActionType
{
    Verification,
    Validated
}

public enum ValidatedType
{
    RequestedBallot,
    Validation
}

public enum CnpType
{
    MrzScan = 1,
    ManualEntry,
    UeCitizen,
    SpecialBallot,
    SpecialBallotUeCitizen,
    Corespondent,
}


public static class ElectionType
{

    public static readonly Dictionary<int, Guid> ElectionMapping = new Dictionary<int, Guid>()
    {
        //Europarlamentare
        {22, Guid.Parse("110d0874-a821-4de5-a06a-50b21082c804")},
        
        //Locale
        {23, Guid.Parse("83c3a603-2c08-4750-8a43-5d791f02fda9")}
    };
}

    