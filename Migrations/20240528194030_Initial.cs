﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SIMPVSync.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "A_Alegatori_Simpv",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TipLista = table.Column<int>(type: "int", nullable: false),
                    PozitiePagina = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DrepturiRevocate = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Cnp = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Prenume = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nume = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DataNasterii = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Cetatenie = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_A_Alegatori_Simpv", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "D_NOTIFICARI_SECTII",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    XDM_FNO = table.Column<int>(type: "int", nullable: true),
                    XDM_FID = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    XDM_CNO = table.Column<int>(type: "int", nullable: true),
                    XDM_CID = table.Column<int>(type: "int", nullable: true),
                    ID_Z_JUDET = table.Column<int>(type: "int", nullable: true),
                    ID_Z_UAT = table.Column<int>(type: "int", nullable: true),
                    ID_SECTIE_VOTARE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ID_Z_SECTIE_VOTARE = table.Column<int>(type: "int", nullable: true),
                    DESCRIERE = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ID_Z_STATUS_NOTIFICARE = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    OBSERVATII = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DATA_CREAT = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DATA_MODIFICAT = table.Column<DateTime>(type: "datetime2", nullable: true),
                    USER_CREAT = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    USER_MODIFICAT = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    IP_CREAT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IP_MODIFICAT = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NRTRANZ = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RETRAS = table.Column<bool>(type: "bit", nullable: true),
                    STRUCTURA_CREAT = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    STRUCTURA_MODIFICAT = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DECLANSATOR_VERS = table.Column<bool>(type: "bit", nullable: true),
                    ID_Z_JUDET_COMPETENTA = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ID_Z_ALEGEGERI = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ID_Z_TIP_NOTIFICARE_ALEGERI = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    XDM_FID_EVENIMENT = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    COD_NOTIF = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    TipNotificare = table.Column<int>(type: "int", nullable: false),
                    NrSectie = table.Column<int>(type: "int", nullable: true),
                    JudetScurt = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Judet = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Uat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Localitate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumeSectie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdresaSectie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TelefonSectie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdPersoana = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_D_NOTIFICARI_SECTII", x => x.ID);
                    table.ForeignKey(
                        name: "FK_D_NOTIFICARI_SECTII_A_Alegatori_Simpv_IdPersoana",
                        column: x => x.IdPersoana,
                        principalTable: "A_Alegatori_Simpv",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "A_Evenimente_Simpv",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tip = table.Column<int>(type: "int", nullable: false),
                    TipScanare = table.Column<int>(type: "int", nullable: false),
                    Sms = table.Column<int>(type: "int", nullable: false),
                    Offline = table.Column<bool>(type: "bit", nullable: false),
                    IdSectie = table.Column<int>(type: "int", nullable: false),
                    Judet = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NrSectie = table.Column<int>(type: "int", nullable: true),
                    NumeSectie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdresaSectie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataCreare = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Validat = table.Column<int>(type: "int", nullable: true),
                    DataValidare = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdNotificare = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_A_Evenimente_Simpv", x => x.Id);
                    table.ForeignKey(
                        name: "FK_A_Evenimente_Simpv_D_NOTIFICARI_SECTII_IdNotificare",
                        column: x => x.IdNotificare,
                        principalTable: "D_NOTIFICARI_SECTII",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_A_Alegatori_Simpv_Cnp",
                table: "A_Alegatori_Simpv",
                column: "Cnp",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_A_Evenimente_Simpv_IdNotificare",
                table: "A_Evenimente_Simpv",
                column: "IdNotificare");

            migrationBuilder.CreateIndex(
                name: "IX_D_NOTIFICARI_SECTII_IdPersoana",
                table: "D_NOTIFICARI_SECTII",
                column: "IdPersoana");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "A_Evenimente_Simpv");

            migrationBuilder.DropTable(
                name: "D_NOTIFICARI_SECTII");

            migrationBuilder.DropTable(
                name: "A_Alegatori_Simpv");
        }
    }
}
